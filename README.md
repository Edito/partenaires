# Contenu du site Guichet Partenaires

Ce projet permet d'héberger l'ensemble des contenus du site [Guichet Partenaires](https://www.guichet-partenaires.fr).

[![Latest Version](https://img.shields.io/pypi/v/upref.svg?style=flat)](https://pypi.python.org/pypi/upref/)
[![License](https://img.shields.io/github/license/IIXIXII/upref.svg?style=flat)](https://github.com/IIXIXII/upref/blob/master/LICENSE.md)
[![Build Status](https://img.shields.io/travis/IIXIXII/upref/master.svg?style=flat)](https://travis-ci.org/IIXIXII/upref)
[![Documentation Status](https://img.shields.io/readthedocs/upref.svg?style=flat)](https://upref.readthedocs.io/en/latest/?badge=latest)
