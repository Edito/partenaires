<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(description)="FAQ" -->
<!-- var(keywords)="FAQ" -->
<!-- var(collapsable)="close" -->


Foire Aux Questions 
===================

## Mon compte

### J'ai oublié mon mot de passe <!-- collapsable:close -->

Si vous avez oublié votre mot de passe, veuillez [le renouveler](https://account.guichet-partenaires.fr/users/renew). Saisissez l'adresse courriel que vous utilisez sur guichet-partenaires.fr et vous recevrez par courriel un lien à usage unique qui vous permettra de renouveler votre mot de passe. Le lien restera valable pendant 24 heures.

<p align="center">![Renouvellement_mot_de_passe](FAQ_renouvellement_mot_de_passe.png)
<br>

### Puis-je modifier mes informations de contact ? <!-- collapsable:close -->

Vous pouvez accéder à votre compte depuis votre espace partenaire **en cliquant sur le bouton « Modifier mon compte »** en haut à droite de la page.

<p align="center">![modifier_compte](FAQ_modifier_compte.png)
<br>

Vous aurez alors la possibilité de changer vos informations personnelles.

<p align="center">![informations_compte](FAQ_informations_compte.png)
<br>

Pour modifier votre mot de passe, veuillez [suivre la procédure d’oubli de mot de passe](https://account.guichet-partenaires.fr/users/renew).

### Mon référent m'a créé un compte mais je ne peux pas me connecter <!-- collapsable:close -->

Si vous ne pouvez pas accéder au compte créé par votre référent, veuillez [renouveler votre mot de passe](https://account.guichet-partenaires.fr/users/renew). Si malgré cela vous ne parvenez toujours pas à vous connecter, [veuillez consulter l'assistance utilisateur](__footer__/contact.md).

### Je suis référent mais je n'ai pas reçu le courriel d'activation <!-- collapsable:close -->

Si vous n'avez pas reçu la courriel d'activation, [veuillez consulter l'assistance utilisateur](__footer__/contact.md).

## Mon espace partenaire

### Je dispose d'un compte sur guichet-partenaires.fr mais je ne vois pas mes dossiers sur mon espace partenaire <!-- collapsable:close -->

Si vous ne pouvez pas accéder aux dossiers de votre compte, [veuillez consulter l'assistance utilisateur](__footer__/contact.md).

### Je suis tête de réseau mais je ne peux pas visualiser la liste des agents de mon réseau <!-- collapsable:close -->

Si vous ne pouvez pas visualiser la liste des agents de votre réseau, [veuillez consulter l'assistance utilisateur](__footer__/contact.md).

### Je ne parviens pas à visualiser les fichiers PDF de mes dossiers <!-- collapsable:close -->

Si vous ne parvenez pas à visualiser les fichiers PDF de vos dossiers depuis un navigateur autre que Mozilla Firefox ou Google Chrome, nous vous recommandons d'utiliser une version récente de l'un de ces deux navigateurs. Si malgré cela vous ne parvenez toujours pas à les visualiser, [veuillez consulter l'assistance utilisateur](__footer__/contact.md).