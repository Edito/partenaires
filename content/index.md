<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(description)="la description de la page" -->
<!-- var(keywords)="liste des mots clefs avec un séparateur à définir" -->

<!-- var(site:home)="Accueil" -->
<!-- var(site:footer)="__footer__/__links__.md" -->
<!-- var(site:menu)="__menu__.md" -->
<!-- var(site:ribbon_off)="off" -->


Le site des organismes partenaires du service Guichet Entreprises<!-- section-stories:arbres.jpg --> <!-- color:dark -->
=========================================

Le site guichet-partenaires.fr est conçu pour les organismes destinataires des dossiers créés par les déclarants sur les sites du service Guichet Entreprises.

Simple, rapide et sécurisé, votre espace partenaire sur guichet-partenaires.fr vous permet de recevoir, consulter en ligne, télécharger et archiver tous les dossiers déposés par les déclarants.

Il facilitera votre travail et le traitement des dossiers dont vous êtes destinataire, et vous permettra d'avoir une vision globale des dossiers que vous avez traités et qu'il vous reste à traiter.


Les sites du Guichet Entreprises <!-- section-information:-80px -->
===================================

[Guichet Entreprises](https://www.guichet-entreprises.fr/)
-----------------------------------------------------------------
permet d’accomplir, à distance et par voie électronique, les formalités nécessaires à la création, aux modifications de situation et à la cessation d’activité d’une entreprise.

[Guichet Qualifications](https://www.guichet-qualifications.fr/)
-----------------------------------------------------------------------
permet aux ressortissants de l'Union européenne et de l'Espace économique européen de faire reconnaître leurs qualifications professionnelles en France.

[Guichet Partenaires](https://www.guichet-partenaires.fr/)
-----------------------------------------------------------------------
permet aux organismes destinataires de recevoir, consulter en ligne, télécharger et archiver tous les dossiers déposés par les déclarants.


Créér un compte et s'identifier<!-- section-welcome: -->
===============================

Si vous êtes référent de votre réseau, vous devriez avoir reçu un courriel contenant un lien vous permettant de créer puis de valider un compte sur guichet-partenaires.fr. Il vous suffit alors de cliquer sur le lien et de suivre les étapes de la formalité de création de compte.

Si vous êtes agent d'un organisme partenaire du service Guichet Entreprises et que vous souhaitez instruire des dossiers depuis le site guichet-partenaires.fr, vous devez faire une demande de création de compte auprès de votre référent réseau, seule personne habilitée à effectuer cette action.

Votre espace partenaire <!-- section:banner --><!-- color:dark -->
===================

Les fonctionnalités


Les fonctionnalités <!-- section-information:-300px -->
===================================

Instruire
-------
Accéder aux dossiers des déclarants, les télécharger, les instruire et les archiver.

Gérer
------
Créer un nouvel utilisateur et paramétrer l'organisation.

S'informer
----------
Consulter la foire aux questions ou contacter le service Guichet Entreprises.

Paramétrer
----------
Modifier vos paramètres de réception et vos coordonnées de transmission.


D'autres fonctionnalités vous seront proposées très prochainement :<!-- section-welcome: -->
=================================================================

* Mettre à jour des informations de paiement
* Proposer des modifications de contenu
* Informer directement le déclarant
* Partager votre expérience utilisateur


Le service Guichet Entreprises en chiffres <!-- section-welcome: -->
====================================================================

61955
-----
C'est le nombre de dossiers constitués sur le site [<font style="text-transform: lowercase;">www.guichet-entreprises.fr</font>](https://www.guichet-entreprises.fr/) en 2017.

100332
-------
C'est le nombre de dossiers constitués sur le site [<font style="text-transform: lowercase;">www.guichet-entreprises.fr</font>](https://www.guichet-entreprises.fr/) en 2018.

9
-------
C'est le pourcentage de création d'entreprise réalisé sur le site  [<font style="text-transform: lowercase;">www.guichet-entreprises.fr</font>](https://www.guichet-entreprises.fr/) par rapport à l'ensemble des créations d'entreprise en France en 2017 (591 000).

13
-------
C'est le pourcentage de création d'entreprise réalisé sur le site [<font style="text-transform: lowercase;">www.guichet-entreprises.fr</font>](https://www.guichet-entreprises.fr/) par rapport à l'ensemble des créations d'entreprise en France en 2018 (691 000).

Des questions ?<!-- section-welcome: -->
=========================================

Vous avez des questions sur le fonctionnement du site ou des aspects techniques ? Vous n'avez pas reçu le courriel d'invitation ou le lien est inactif ? Nous vous invitons à [<font style="text-transform: lowercase;">consulter la</font> FAQ](faq.md) ou à [<font style="text-transform: lowercase;">contacter l'assistance utilisateur</font>](__footer__/contact.md).
