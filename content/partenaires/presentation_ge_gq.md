<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(description)="Présentation des sites Guichet Entreprises et Guichet Qualifications" -->
<!-- var(keywords)="Guichet Entreprises Guichet Qualifications" -->

<!-- var(page:breadcrumb)="Présentation des sites Guichet Entreprises et Guichet Qualifications" -->
<!-- var(page:title)="Présentation des sites Guichet Entreprises et Guichet Qualifications" -->
<!-- var(collapsable)="close" -->

Présentation des sites Guichet Entreprises et Guichet Qualifications
===================================================================

## Guichet Entreprises

<br/>
<br/>
<p align="center">![guichet-entreprises](GE long - baseline - 200ppp.png)
<br/>
<br/>

Le service en ligne [guichet-entreprises.fr](https://www.guichet-entreprises.fr) encourage la création d’entreprise en France en permettant aux citoyens de réaliser leurs démarches autour de la création d’une activité (immatriculation, demandes d’autorisation, etc.). Il est le site des pouvoirs publics de la création d’entreprise, de la modification et de la cessation d’activité d’une entreprise. Ce service est une initiative du ministère de l’Économie et des Finances. Faciliter les démarches administratives, c’est encourager l’esprit d’entreprendre !

### Faciliter la création d’entreprise <!-- collapsable:close -->

La création d’une entreprise s’accompagne de formalités administratives afin de lui donner une existence juridique. Sur le site [guichet-entreprises.fr](https://www.guichet-entreprises.fr), les déclarants constituent un dossier de création d’entreprise en ligne. Plus besoin de papier, il sera transmis automatiquement au centre de formalités des entreprises (CFE) compétent.

### Le créateur d’entreprise au cœur de la démarche <!-- collapsable:close -->

Le site [guichet-entreprises.fr](https://www.guichet-entreprises.fr) permet une procédure entièrement dématérialisée des formalités liées à la création d’entreprise (immatriculation, autorisation, etc.) quelle que soit la forme juridique (entreprise individuelle ou société). Il permet également aux micro-entrepreneurs de modifier ou cesser l’activité de leur entreprise. Le site est entièrement sécurisé. L'espace personnel des déclarants leur permet de gérer leur dossier, de le sauvegarder, etc.

### Un guichet unique de la création d’entreprise <!-- collapsable:close -->

Créer un dossier sur [guichet-entreprises.fr](https://www.guichet-entreprises.fr) est une démarche simple : il suffit de remplir les champs demandés. Une fois validé, le dossier est réceptionné puis traité par le CFE compétent. Le déclarant reçoit par la suite son numéro Siren, son code APE, son numéro de TVA, etc. Tous les organismes de la création d’entreprise – services fiscaux, Urssaf, Insee, etc. – sont informés !

### Créer un dossier sur guichet-entreprises.fr : mode d'emploi <!-- collapsable:close -->

La création d'un dossier sur [guichet-entreprises.fr](https://www.guichet-entreprises.fr) est très simple et se fait en trois étapes :

1. Création d'un espace personnel (ou connexion avec les identifiants [FranceConnect](https://franceconnect.gouv.fr/)) ;

2. Création de l’entreprise elle-même : le déclarant renseigne le profil de l’entreprise, complète les formulaires, ajoute les pièces jointes, etc. Un récapitulatif lui permet de suivre l’avancement de son dossier ;

3. Validation générale du dossier. Toutes les pièces peuvent ensuite être téléchargées (formulaire Cerfa, etc.). Lorsque le dossier est transmis, le CFE devient l'interlocuteur unique du déclarant.

### Exercer une activité réglementée en France <!-- collapsable:close -->

L’exercice de certaines activités fait l’objet d’une réglementation spécifique en France pour lesquelles une autorisation ou un agrément est nécessaire avant d’exercer la profession.

Pour créer une entreprise, il est nécessaire de respecter certaines règles et formalités particulières (déclaration, autorisation, etc.). Les [fiches d’information](https://www.guichet-entreprises.fr/fr/activites-reglementees/) proposées sur le site [guichet-entreprises.fr](https://www.guichet-entreprises.fr) permettent de faire le point sur ces formalités et la législation concernant ces activités. À ce jour, 105 activités sont détaillées, elles sont regroupées par familles d’activités, consultez-les !

## Guichet Qualifications

<br/>
<br/>
<p align="center">![guichet-qualifications](logo-long800px.png)
<br/>
<br/>

Le service en ligne [guichet-qualifications.fr](https://www.guichet-qualifications.fr) encourage la mobilité professionnelle des résidents de l’Union européenne (UE) et de l’Espace économique européen (EEE) en offrant une information complète sur l’accès et l’exercice des professions réglementées en France, en vue d’une reconnaissance de qualification professionnelle. Ce service est une initiative du ministère de l’Économie et des Finances.

### Exercer une profession réglementée en France avec un diplôme européen <!-- collapsable:close -->

En France, certaines professions nécessitent pour leur exercice l’obtention d’un titre professionnel, d’une qualification professionnelle, voire d’une autorisation préalable, sanctionnant un certain niveau de formation ou d’expérience. Si un ressortissant de l'UE ou de l'EEE a obtenu un diplôme dans un autre État membre de l’UE ou de l’EEE et qu'il souhaite exercer en France, il doit obtenir une autorisation auprès de l’autorité compétente. Le site [guichet-qualifications.fr](https://www.guichet-qualifications.fr) fait le point sur les informations essentielles pour effectuer ces démarches (fiches d'information sur les [professions réglementées](https://www.guichet-qualifications.fr/fr/professions-reglementees/), coordonnées des autorités compétentes et des centres d'assistance, etc.). 

### Effectuer une reconnaissance de qualification professionnelle en ligne <!-- collapsable:close -->

Le site [guichet-qualifications.fr](https://www.guichet-qualifications.fr) permet aux ressortissants de l'UE et de l'EEE d’accomplir les formalités de reconnaissance des qualifications professionnelles acquises dans un autre État membre pour l’exercice d’une profession réglementée en France. Plus besoin de papier : il suffit au déclarant de constituer un dossier en ligne qui sera transmis directement à l’autorité compétente. 


## Présentation du service Guichet Entreprises par Florent Tournois

<br>
<figure>
<center><iframe width="560" height="315" src="https://www.youtube.com/embed/pgo8-t9ubj0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center>
<figcaption><p align=center><strong><i>Présentation du service Guichet Entreprises par Florent Tournois</i></strong></p></figcaption>
