<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(description)="Réception des dossiers" -->
<!-- var(keywords)="Réception des dossiers" -->

<!-- var(page:breadcrumb)="Réception des dossiers" -->
<!-- var(page:title)="Réception des dossiers" -->
<!-- var(collapsable)="close" -->

Réception des dossiers
========================

Le site guichet-partenaires.fr vous permet de recevoir et traiter les dossiers déposés par les déclarants sur les sites [guichet-entreprises.fr](https://www.guichet-entreprises.fr) et [guichet-qualifications.fr](https://www.guichet-qualifications.fr).

## Choisir son canal de réception <!-- collapsable:close -->

Si vous êtes référent, vous avez la possibilité de choisir le canal de réception qui convient le mieux à votre organisme parmi les quatre canaux suivants :

* le canal de réception par courrier avec accusé de réception ;
* le canal courriel ;
* le canal FTP Eddie ;
* le canal *Backoffice* du site guichet-partenaires.fr.

Pour configurer le canal de réception des dossiers déposés par les déclarants sur les sites [guichet-entreprises.fr](https://www.guichet-entreprises.fr/) et [guichet-qualifications.fr](https://www.guichet-qualifications.fr/), veuillez consulter la page consacrée à la [configuration du canal de réception des dossiers](../espace_partenaire/configuration_reception.md).

## Traitement des dossiers <!-- collapsable:close -->

Une fois votre compte créé et validé sur guichet-partenaires.fr, vous serez redirigé vers votre espace partenaire. Celui-ci se présente sous la forme d'un tableau de bord affichant les dossiers dont vous avez la charge. Chaque dossier comporte une référence, la date de transmission, la dénomination (exemple : déclaration de début d'activité agricole), ainsi que son état (« À traiter » et « Archivé »).

<p align="center">![Dashboard_GP](reception_dossiers_dashboard_GP.png)
<p align="center">*Le tableau de bord sur guichet-partenaires.fr*
<br>

Selon l'état des dossiers dont vous êtes responsable, jusqu'à trois actions vous sont proposées : « Consulter », « Télécharger » et « Archiver ».

<p align="center">![Boutons_dossier](reception_dossiers_boutons.PNG)
<p align="center">*Les actions possibles sur les dossiers des déclarants*

## Délais légaux <!-- collapsable:close -->

Conformément aux articles [R. 123-25](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006256049&dateTexte=&categorieLien=cid) et [R. 123-9](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006255899&dateTexte=&categorieLien=cid) du Code de commerce, les organismes destinataires sont tenus de traiter les dossiers transmis par le service Guichet Entreprises. Conformément à l'article [R. 123-9](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006255899&dateTexte=&categorieLien=cid) du Code de commerce, « le centre de formalités des entreprises compétent, saisi du dossier complet conformément aux dispositions de l'article [R. 123-7](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006255891&dateTexte=&categorieLien=cid), transmet le jour même aux organismes destinataires, et le cas échéant aux autorités habilitées à délivrer les autorisations, informations et pièces les concernant. »
