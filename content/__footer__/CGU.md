<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(description)="Conditions générales d'utilisation" -->
<!-- var(keywords)="Conditions générales utilisation" -->

<!-- var(page:breadcrumb)="Condition générales d'utilisation" -->
<!-- var(page:title)="Conditions générales d'utilisation" -->
<!-- var(collapsable)="close" -->
   

Conditions générales d'utilisation
==================================
Préambule <!-- collapsable:close -->
---------

Ce document présente les modalités d’engagement à l’utilisation du téléservice Guichet Partenaires pour les partenaires. Il s’inscrit dans le cadre juridique :
* de la directive 2006/123/CE du Parlement européen et du Conseil du 12 décembre 2006 relative aux services dans le marché intérieur ;<br/><br/>
* du règlement n° 910/2014 du Parlement européen et du Conseil du 23 juillet 2014 (e-IDAS) sur l’identification électronique et les services de confiance pour les transactions électroniques au sein du marché intérieur ;<br/><br/>
* du règlement n° 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif au traitement des données à caractère personnel ;<br/><br/>
* de l’ordonnance du 8 décembre 2005 relative aux échanges électroniques entre les usagers et les autorités administratives et entre les autorités administratives et le décret n° 2010-112 du 2 février 2010 pris pour l’application des articles 9, 10 et 12 de cette ordonnance ;<br/><br/>
* du dispositif de la loi n° 78-17 du 6 janvier 1978 modifiée relative à l’informatique, aux fichiers et aux libertés ;<br/><br/>
* de l’article 441-1 du Code Pénal ;<br/><br/>
* des articles R. 123-1 à R123-28 du Code de commerce ;<br/><br/>
* de l’arrêté du 22 avril 2015 portant création d’un service à compétence nationale dénommé « guichet entreprises ».

Objet du document <!-- collapsable:close -->
-----------------

Le présent document a pour objet de définir les conditions générales d’utilisation du service Guichet Partenaires, dénommé « service » ci-après, entre le service à compétence nationale Guichet Entreprises et les organismes partenaires.

Le service à compétence nationale Guichet Entreprises est placé sous l’autorité de la direction générale des entreprises du ministère de l’Économie et des Finances.

Définition et objet du service <!-- collapsable:close -->
------------------------------

Le service mis en œuvre par le service à compétence nationale Guichet Entreprises (ci-après dénommé « le service Guichet Entreprises ») contribue à simplifier les démarches liées à la création, aux modifications de la situation et à la cessation d’activité d’une entreprise des usagers français et européens.

L’utilisation du service est facultative et gratuite.

L’ensemble des destinataires du service sont dénommées ci-après les partenaires.

Fonctionnalités <!-- collapsable:close -->
---------------

Le service permet au partenaire :

* d’accéder à la documentation précisant les fonctionnalités disponibles ;<br/><br/>
* de créer un compte utilisateur donnant accès à un espace de stockage personnel. Cet espace permet au partenaire de gérer et utiliser ses données à caractère personnel, et de conserver les informations le concernant.

Depuis son espace, l’usager peut :

* visualiser, traiter, télécharger et archiver les dossiers des déclarants qui le concernent ;
* pour les référents des organismes partenaires, créer un nouvel utilisateur ;
* pour les référents des organismes partenaires, mettre à jour les informations de leur organisme ;
* pour les référents des organismes partenaires, configurer le canal de réception de leur organisme ;
* pour les référents paiement des organismes partenaires, mettre à jour les informations de paiement de leur organisme.

Modalités d’inscription et d’utilisation du service <!-- collapsable:close -->
---------------------------------------------------

L’accès au service est  gratuit et ouvert à toute personne autorisée. Il est facultatif et n’est pas exclusif d’autres canaux d’accès pour permettre au partenaire de recevoir les dossiers des déclarants qui le concernent.

Pour la gestion de l’accès à l’espace personnel du service, le partenaire partage les informations suivantes :

* l’adresse électronique du partenaire ;
* le mot de passe choisi par le partenaire.

Le traitement des données personnelles est décrit dans la [politique de protection des données du site [guichet.entreprises.fr](https://www.guichet-entreprises.fr/fr/politique-de-protection-des-donnees-personnelles/).

L’utilisation du service requiert une connexion et un navigateur internet. Le navigateur doit être configuré pour autoriser les *cookies* de session.

Afin de garantir une expérience de navigation optimale, nous vous recommandons d’utiliser les versions de navigateurs suivantes :

* Firefox version 45 et plus ;
* Google Chrome version 48 et plus.

En effet, d'autres navigateurs sont susceptibles de ne pas supporter certaines fonctionnalités du service.

Le service est optimisé pour un affichage en 1024×768 pixels. Il est recommandé d’utiliser la dernière version du navigateur et de le mettre à jour régulièrement pour bénéficier des correctifs de sécurité et des meilleures performances.

Rôles et engagement <!-- collapsable:close -->
-------------------

### Engagement du service Guichet Entreprises

 1. Le service Guichet Entreprises met en œuvre et opère le service conformément au cadre juridique en vigueur défini en préambule.<br/><br/>
 2. Le service Guichet Entreprises s’engage à prendre toutes les mesures nécessaires permettant de garantir la sécurité et la confidentialité des informations fournies par l’usager.<br/><br/>
 3. Le service Guichet Entreprises s’engage à assurer la protection des données collectées dans le cadre du service, et notamment empêcher qu’elles soient déformées, endommagées ou que des tiers non autorisés y aient accès, conformément aux mesures prévues par l’ordonnance du 8 décembre 2005 relative aux échanges électroniques entre les usagers et les autorités administratives et entre les autorités administratives, le décret n° 2010-112 du 2 février 2010 pris pour l’application des articles 9, 10 et 12 de cette ordonnance et le règlement n° 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif au traitement des données à caractère personnel.<br/><br/>
 4. Le service Guichet Entreprises et les organismes partenaires garantissent aux usagers du service les droits d’accès, de rectification et d’opposition prévus par la loi n° 78-17 du 6 janvier 1978 relative à l’informatique aux fichiers et aux libertés et le réglement n° 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif au traitement des données à caractère personnel.<br/>
 Ce droit peut s’exercer de plusieurs façons :<br/>
 a- en envoyant un courriel au [support](mailto:support.guichet-entreprises\@helpline.fr) ;<br/>
 b- en envoyant un courrier à l'adresse suivante :<br/>
   <span class="TabulTexte">Service à compétence nationale Guichet Entreprises<br/></span>
   <span class="TabulTexte">120 rue de Bercy – Télédoc 766<br/></span>
   <span class="TabulTexte">75572 Paris cedex 12</span><br/>
 5. Le service Guichet Entreprises et les organismes partenaires s’engagent à n’opérer aucune commercialisation des informations et documents transmis par l’usager au moyen du service, et à ne pas les communiquer à des tiers, en dehors des cas prévus par la loi.<br/><br/>
 6. Le service Guichet Entreprises s’engage à assurer la traçabilité de toutes les actions réalisées par l’ensemble des utilisateurs du service.<br/><br/>
 7. Le service Guichet Entreprises offre aux usagers un support en cas d’incident ou d’alerte sécurité défini.

### Engagement du partenaire

Les présentes conditions générales s’imposent à tout utilisateur usager du service.

Disponibilité et évolution du service <!-- collapsable:close -->
-------------------------------------

Le service est disponible 7 jours sur 7, 24 heures sur 24. Le service Guichet Entreprises se réserve toutefois la faculté de faire évoluer, de modifier ou de suspendre sans préavis le service pour des raisons de maintenance ou pour tout autre motif jugé nécessaire. L’indisponibilité du service ne donne droit à aucune indemnité. En cas d’indisponibilité du service, une page d’information est alors affichée au partenaire lui mentionnant cette indisponibilité ; il est alors invité à effectuer sa démarche ultérieurement.

Les termes des présentes conditions d’utilisation peuvent être amendés à tout moment, sans préavis, en fonction des modifications apportées au service, de l’évolution de la législation ou de la réglementation, ou pour tout autre motif jugé nécessaire.

Responsabilités <!-- collapsable:close -->
---------------

 1. La responsabilité du service Guichet Entreprises ne peut être engagée en cas d’usurpation d’identité ou de toute utilisation frauduleuse du service.<br/><br/>
 2. Le partenaire peut à tout moment modifier les données transmises au service Guichet Entreprises ou les supprimer. Il peut choisir de supprimer toutes les informations de son compte en supprimant ses données auprès du service.<br/><br/>