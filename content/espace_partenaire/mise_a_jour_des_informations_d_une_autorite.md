<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(description)="Mettre à jour les informations d'une autorité" -->
<!-- var(keywords)="mise à jour informations autorité" -->

<!-- var(page:breadcrumb)="Mettre à jour les informations d'une autorité" -->
<!-- var(page:title)="Mettre à jour les informations d'une autorité" -->
<!-- var(collapsable)="close" -->

Mettre à jour les informations d'une autorité
=============================================

## Sélection de la formalité <!-- collapsable:close -->

Pour mettre à jour les informations de votre autorité, veuillez vous rendre sur votre espace partenaire puis **cliquer sur le bouton « Services »** situé en haut à droite de la page (cf. Image 1). Veuillez ensuite sélectionner la formalité « Mise à jour du profil d'une autorité » dans la page des services (cf. Image 2).
<p align="center">![selection_formalite](1_selection_formalite.png)
<p align="center">*Image 1*
<p align="center">![page_service](MAJ_infos_page_services.PNG)
<p align="center">*Image 2*

## Sélection de l'autorité <!-- collapsable:close -->

Veuillez ensuite **saisir l'autorité à configurer dans le menu déroulant** et cliquer sur le bouton « Étape suivante ».

<br>
<p align="center">![Selection_autorite](MAJ_infos_selection_autorite.png)
<br>

## Modification des informations <!-- collapsable:close -->

Dans l'écran de mise à jour des informations qui apparaît, vous pouvez modifier les informations dans les blocs suivants :

-   le bloc « Profil de l'autorité » pour modifier le numéro de téléphone, le numéro de fax, le courriel ainsi que le site web de l'autorité ;

<br>
<p align="center">![Modification_profil](MAJ_infos_modification_profil.png)
<br>

-    le bloc « Adresse » pour modifier l'adresse de l'autorité.

<br>
<p align="center">![Modification_adresse](MAJ_infos_modification_adresse.png)
<br>

Une fois les modifications effectuées, **veuillez cliquer sur le bouton « Étape suivante ».**

## Enregistrement des modifications <!-- collapsable:close -->

Un message vous confirmant la prise en compte des modifications ainsi qu'un récapitulatif des informations renseignées s'affichent. Il ne vous reste qu'à **cliquer sur le bouton « Finaliser »** pour valider la modification des informations.

<br>
<p align="center">![Modifications_enregistrees](MAJ_infos_modifications_enregistrees.png)