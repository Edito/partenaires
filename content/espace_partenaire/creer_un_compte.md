<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(description)="Créer un compte" -->
<!-- var(keywords)="Créer un compte" -->

<!-- var(page:breadcrumb)="Créer un compte" -->
<!-- var(page:title)="Créer un compte" -->
<!-- var(collapsable)="close" -->

Créer un compte
==============

## Pour les utilisateurs

Si vous êtes agent d'un organisme partenaire du service Guichet Entreprises et que vous désirez instruire des dossiers depuis le site guichet-partenaires.fr, vous devez faire une demande de création de compte auprès de votre référent réseau, seule personne habilitée à effectuer cette action.

## Pour les référents

Si le service Guichet Entreprises vous a identifié comme « référent » de votre réseau, vous devriez avoir reçu un courriel contenant un lien vous permettant de créer puis de valider un compte sur guichet-partenaires.fr. Si vous n'avez pas reçu le courriel ou que le lien est inactif, veuillez [contacter l'assistance utilisateur](../__footer__/contact.md).

Une fois votre compte créé, vous aurez la possibilité de créer des comptes « utilisateur », « référent » et « référent système de paiement » pour les agents de votre organisme.

### Créer un nouveau compte <!-- collapsable:close -->

Pour cela, il vous suffit de cliquer sur le bouton « Services » situé en haut à droite de votre tableau de bord (cf. Image 1) puis de sélectionner la formalité « Association d'un partenaire à un utilisateur » dans la page des services (cf. Image 2).

<p align="center">![selection_formalite](1_selection_formalite.png)
<p align="center">*Image 1*
<p align="center">![page_service](page_services.PNG)
<p align="center">*Image 2*

### Sélection de l'autorité <!-- collapsable:close -->

Veuillez ensuite saisir le code de l'autorité pour laquelle vous souhaitez créer un compte, rentrer l'adresse courriel du futur utilisateur, sélectionner son type de profil dans le menu déroulant puis cliquer sur « Etape suivante ».

<p align="center">![creation_user_selection_autorite](creation_user_selection_autorite.png)

### Renseignement des informations sur l'utilisateur <!-- collapsable:close -->

Pour finaliser la création du compte, veuillez rentrer les informations sur le futur utilisateur (civilité, nom, prénom et numéro de téléphone) puis cliquer sur « Finaliser » : le compte du nouvel utilisateur est créé. Un mail d'activation lui sera envoyé sur l'adresse courriel renseignée.

<p align="center">![creation_user_parametrage_compte](creation_user_parametrage_compte.PNG)

Pour plus d'informations sur les différents profils du site Guichet Partenaires, veuillez consulter la page « [Services](../espace_partenaire/services.md) ».

