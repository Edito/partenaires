<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(description)="Configuration du canal de réception des dossiers" -->
<!-- var(keywords)="Configuration canal réception" -->

<!-- var(page:breadcrumb)="Configurer le canal de réception des dossiers" -->
<!-- var(page:title)="Configurer le canal de réception des dossiers" -->
<!-- var(collapsable)="close" -->

Configurer le canal de réception des dossiers
=========================================

Si vous êtes référent, vous avez la possibilité de choisir le canal de réception qui convient le mieux à votre organisme parmi les quatre canaux suivants :

* le canal de réception par courrier avec accusé de réception ;
* le canal courriel ;
* le canal FTP Eddie ;
* le canal *Backoffice* du site guichet-partenaires.fr.

Pour configurer le canal de réception des dossiers déposés par les déclarants sur les sites [guichet-entreprises.fr](https://www.guichet-entreprises.fr/) et [guichet-qualifications.fr](https://www.guichet-qualifications.fr/), veuillez suivre les instructions suivantes :

## Sélection de la formalité <!-- collapsable:close -->

Pour configurer le canal de réception de votre organisme, veuillez vous rendre sur votre espace partenaire puis **cliquer sur le bouton « Services »** situé en haut à droite de votre espace partenaire (cf. Image 1). Veuillez ensuite sélectionner la formalité « Gestion des canaux de réception des dossiers » dans la page des services (cf. Image 2).

<p align="center">![selection_formalite](1_selection_formalite.png)
<p align="center">*Image 1*
<p align="center">![page_service](page_services.PNG)
<p align="center">*Image 2*
## Sélection de l'autorité à configurer <!-- collapsable:close --> 

Veuillez ensuite **sélectionner l'autorité compétente que vous souhaitez configurer dans le menu déroulant** :

<p align="center">![selection_autorite](canal_selection_autorite.PNG)

## Sélection du canal de réception <!-- collapsable:close -->

Vous pouvez **choisir votre mode de réception** parmi les quatre modes proposés dans le menu « Choix du canal à configurer » :

<p align="center">![selection_canal](canal_selection_canal.PNG)

### Recevoir les dossiers par courrier postal <!-- collapsable:close --> 

Pour recevoir les dossiers par courrier postal, après avoir cliqué sur le bouton « Vous souhaitez recevoir le dossier par courrier postal » dans le menu « Choix du canal à configurer », **veuillez cocher la case « Activer ce canal » dans l'écran qui s'affiche et renseigner les informations de votre organisme** :

<p align="center">![courrier_postal](canal_courrier_postal.PNG)

### Recevoir les dossiers par courrier électronique <!-- collapsable:close --> 

Pour recevoir les dossiers par courrier électronique, après avoir cliqué sur le bouton « Vous souhaitez recevoir le dossier par courrier électronique » dans le menu « Choix du canal à configurer »**, veuillez cocher la case « Activer ce canal » dans l'écran qui s'affiche et renseigner l'adresse courriel de votre organisme** :

<p align="center">![courriel](canal_courriel.png)

### Recevoir les dossiers dans le *Backoffice* partenaires <!-- collapsable:close --> 

Pour recevoir les dossiers dans votre espace *backoffice* partenaires, après avoir cliqué sur le bouton « Vous souhaitez recevoir le dossier dans le backoffice partenaires », **veuillez cocher la case « Activer ce canal »** :

<p align="center">![back_office](canal_back_office.png)

### Recevoir les dossiers via le canal FTP <!-- collapsable:close --> 

Pour recevoir les dossiers via le canal FTP, après avoir cliqué sur le bouton « Vous souhaitez recevoir le dossier via FTP », trois options s'offrent à vous :

* recevoir les dossiers sur le serveur FTP de votre autorité ;
* récupérer les dossiers sur le serveur FTP du service Guichet Entreprises ;
* utiliser le canal FTP d'une autre autorité.

<p align="center">![FTP](canal_FTP.png)

#### Recevoir les dossiers sur le serveur FTP de votre autorité

Pour recevoir les dossiers sur le serveur FTP de votre autorité, deux modes d'authentification s'offrent à vous :

* l'authentification par nom d'utilisateur et mot de passe ;
* l'authentification par clé RSA.

<p align="center">![FTP_push](canal_FTP_push.png)

##### Nom d'utilisateur et mot de passe

Si vous avez choisi l'option nom d'utilisateur/mot de passe, **veuillez cocher la case « Activer ce canal » et saisir les informations suivantes** :

* l'identifiant du canal FTP ;
* un commentaire ;
* le chemin d'un sous-dossier dans le serveur FTP ;
* l'URL de votre serveur FTP ainsi que le numéro de port ;
* le nom d'utilisateur avec lequel le service Guichet Entreprises se connectera à votre serveur FTP ;
* le mot de passe avec lequel le service Guichet Entreprises se connectera à votre serveur FTP.

<p align="center">![FTP_login](canal_FTP_push_login.png)

##### Clé RSA

Si vous avez choisi l'option clé RSA, **veuillez cocher la case « Activer ce canal » et saisir les informations suivantes** :

* l'identifiant du canal FTP ;
* un commentaire ;
* le chemin d'un sous-dossier dans le serveur FTP ;
* l'URL de votre serveur FTP ainsi que le numéro de port ;
* le nom d'utilisateur avec lequel le service Guichet Entreprises se connectera à votre serveur FTP.

<p align="center">![FTP_clef_RSA](canal_FTP_push_clef_rsa.png)

#### Récupérer les dossiers sur le serveur FTP du service Guichet Entreprises

Pour recevoir les dossiers sur le serveur FTP de votre autorité, deux modes d'authentification s'offrent à vous :

* l'authentification par nom d'utilisateur et mot de passe ;
* l'authentification par clé RSA.

<p align="center">![FTP_pull](canal_FTP_pull.png)

##### Nom d'utilisateur et mot de passe

Si vous avez choisi l'option nom d'utilisateur/mot de passe, **veuillez cocher la case « Activer ce canal » et saisir les informations suivantes** :

* l'identifiant du canal FTP ;
* un commentaire ;
* le chemin d'un sous-dossier dans le serveur FTP ;
* le mot de passe de votre compte FTP.

<p align="center">![FTP_pull_login](canal_FTP_pull_login.png)

##### Clé RSA

Si vous avez choisi l'option clé RSA, **veuillez cocher la case « Activer ce canal » et saisir les informations suivantes** :

* l'identifiant du canal FTP ;
* un commentaire (optionnel) ;
* le chemin d'un sous-dossier dans le serveur FTP ;
* votre clé publique RSA.

<p align="center">![FTP_pull_clef_rsa](canal_FTP_pull_clef_rsa.png)

#### Utiliser le canal FTP d'une autre autorité

Pour recevoir les dossiers sur le serveur FTP d'une autre autorité, **veuillez cliquer sur « Vous souhaitez utiliser la canal FTP d'une autre autorité », cocher la case « Activer le canal » et renseigner les informations suivantes** :

* le chemin d'un sous-dossier dans le serveur FTP ;
* l'autorité propriétaire du canal ;
* votre autorité.

<p align="center">![FTP_autre_autorité](canal_FTP_autre_autorite.png)

## Écran récapitulatif et validation <!-- collapsable:close -->

Une fois ces étapes terminées, veuillez cliquer sur « Étape suivante ». **Un écran récapitulatif résume l'autorité et le canal sélectionnés. Pour les confirmer, veuillez cliquer sur le bouton « Finaliser »**.

<p align="center">![recapitulatif](canal_recapitulatif.png)

**Le canal de réception de votre autorité est maintenant configuré.**