<!-- include-file(ge.txt)
+-----------------------------------------------------------------------------+
| 
|    _____       _      _          _     ______       _                       _               
|   / ____|     (_)    | |        | |   |  ____|     | |                     (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__   _ __ | |_ _ __ ___ _ __  _ __ _ ___  ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  __| | '_ \| __| '__/ _ \ '_ \| '__| / __|/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |____| | | | |_| | |  __/ |_) | |  | \__ \  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |______|_| |_|\__|_|  \___| .__/|_|  |_|___/\___||___/
|                                                                 | |                         
|                                                                 |_|                         
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license.en.txt)
+-----------------------------------------------------------------------------+
| 
| Copyright © Guichet Entreprises - All Rights Reserved
|   All Rights Reserved.
|   Unauthorized copying of this file, via any medium is strictly prohibited
|   Dissemination of this information or reproduction of this material
|   is strictly forbidden unless prior written permission is obtained
|   from Guichet Entreprises.
| 
+-------------------------------------------------------------------------- -->
<!-- var(author)="SCN Guichet Entreprises" -->
<!-- var(description)="Services" -->
<!-- var(keywords)="services" -->

<!-- var(page:breadcrumb)="Services" -->
<!-- var(page:title)="Services" -->
<!-- var(collapsable)="close" -->

Services
========

## Les différents services du site Guichet Partenaires

Les différents services du site guichet-partenaires.fr auxquels vous avez accès dépendent de votre profil. Vous trouverez la liste des fonctionnalités disponibles selon les profils ci-dessous.

### Utilisateur <!-- collapsable:close -->

Vous avez été identifié comme utilisateur par votre référent réseau. Sur guichet-partenaires.fr, vous pouvez dès à présent :

* accéder aux dossiers des déclarants et les télécharger ;
* les trier (par date de transmission ou par ordre alphabétique) ;
* les archiver ;
* [nous contacter](..//__footer__/contact.md).

Vous aurez ainsi une visibilité complète sur l’ensemble des dossiers que vous avez traités et qu’il vous reste à traiter. Vous pouvez ainsi administrer et organiser selon vos besoins le traitement des dossiers qui vous sont destinés. 

D'autres fonctionnalités vous seront proposées prochainement, et notamment :

* nous faire remonter votre expérience de navigation et de traitement des dossiers sur guichet-partenaires.fr au moyen d'un formulaire de satisfaction ;
* nous suggérer des modifications de notre site.

### Référent système de paiement <!-- collapsable:close -->

Vous avez été identifié comme « référent système de paiement » par votre référent réseau. Sur guichet-partenaires.fr, vous pouvez désormais mettre à jour les informations de paiement de votre organisme en ligne, de façon simple et rapide, et consulter les dernières écritures pour votre organisme.

### Référent <!-- collapsable:close -->

Vous avez été identifié comme « référent » de votre réseau par le service Guichet Entreprises ou par votre tête de réseau. Sur guichet-partenaires.fr, vous pouvez dès à présent : 

* accéder aux dossiers des déclarants  adressés à votre entité et les télécharger ;
* les trier (par date de transmission ou par ordre alphabétique) ;
* les archiver ;
* [créer un nouveau compte](creer_un_compte.md) ;
* [nous contacter](..//__footer__/contact.md) ;
* [configurer vos canaux de transmission](configuration_reception.md) (dossiers dématérialisés transmis par courriel, le canal FTP, la fonction *back office*, ou en version papier) ;
* [mettre à jour les informations de contact de votre autorité](mise_a_jour_des_informations_d_une_autorite.md).

Vous aurez ainsi une visibilité complète sur l’ensemble des dossiers que votre organisme a traités et ceux qu'il lui reste à traiter. Vous pouvez ainsi administrer et organiser le traitement des dossiers à destination de votre organisme selon ses besoins.

D'autres fonctionnalités vous seront proposées prochainement, et notamment :

* nous faire remonter votre expérience de navigation et de traitement des dossiers sur guichet-partenaires.fr au moyen d'un formulaire de satisfaction ;
* nous suggérer des modifications de notre site.

### Bureaux réglementaires <!-- collapsable:close -->

Si vous faites partie d'un bureau réglementaire, vous pouvez contacter le service Guichet Entreprises afin de nous informer des évolutions réglementaires relatives aux [professions](https://www.guichet-qualifications.fr/fr/professions-reglementees/) et [activités](https://www.guichet-entreprises.fr/fr/activites-reglementees/) réglementées en France.
