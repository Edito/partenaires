# [![Licence Creative Commons](https://i.creativecommons.org/l/by-nd/4.0/80x15.png)](http://creativecommons.org/licenses/by-nd/4.0/)  
Le contenu de [Guichet Partenaires](https://www.guichet-partenaires.fr/) est mis à disposition selon les termes de la [licence Creative Commons Attribution - Pas de Modification 4.0 International](http://creativecommons.org/licenses/by-nd/4.0/).
